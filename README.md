# triplex-mongodb

triplex module that enables you to create a connection to a mongodb server. This connection can then be used by other modules.

## Installation

```sh
npm install triplex-mongodb
```

## Usage

If you want to use this module, you need to add this module to the triplex configuration.

**Hint** You can pass options to triplex in multiple ways. Check the triplex documentation for more information.

```json
{
    "modules" : {
        "triplex-mongodb" : {}
    }
}
```

The module will register a database connection in triplex shared.db object, which will fire an "add" event when the enpoint is ready for use. For more information, check the triplex documentation.

### Options

| Name | Description | Default |
| ---- | ----------- | ------- |
| name | The name of the database. Other modules will use this name to identify which database to use. | "default" |
| url | The url to use for connecting to the databae. | "mongodb://127.0.01:27017" |

### Example

In the example below, a database with name "intranet" is created. Because the url option is not specified, the default url "mongodb://127.0.01:27017" is used.

Then, the dispatcher module is set to use this database.

```json
{
    "modules" : {
        "triplex-endpoint" : {},
        "triplex-mongodb" : {
            "name" : "intranet"
        },
        "triplex-dispatcher" : {
            "db" : "intranet"
        }
    }
}
```
