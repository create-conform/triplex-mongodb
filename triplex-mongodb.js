/////////////////////////////////////////////////////////////////////////////////////////////
//
// Triplex MongoDB
//
//    Triplex module that enables you to use MongoDB databases.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Privates
//
///////////////////////////////////////////////////////////////////////////////////////////// 
    Error =        require("error");
var MongoClient =  require("mongodb").MongoClient;


/////////////////////////////////////////////////////////////////////////////////////////////
//
// TriplexMongoDB Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function TriplexMongoDB(options, shared) {
    if (!(this instanceof TriplexMongoDB)) return new TriplexMongoDB(options, shared);

    var self = this;
    var connection;
    var name;

    shared =           shared || {};
    shared.db =        shared.db || {};

    this.options =     options || {};


    ////////////////////////////////////////////////////////////////////
    //
    // Service Controllers
    //
    ////////////////////////////////////////////////////////////////////
    this.start = function() {
        return new Promise(function(resolve, reject) {
            name = self.options.name || "default";

            if (shared.db[name]) throw new Error(self.ERROR_DB_ALREADY_EXISTS, "A database connection with name '" + name + "' already exists.");

            ////////////////////////////////////////////////////////////////////
            //
            // Init DB Connection
            //
            ////////////////////////////////////////////////////////////////////
            MongoClient.connect(self.options.url || "mongodb://127.0.0.1:27017", function(err, conn) {
                if (err) throw err;
                
                // save connection
                connection = conn;
            
                shared.db.add(name, connection);
            });

            resolve();
        });
    };

    this.stop = function() {
        return new Promise(function(resolve, reject) {
            if (connection) {
                try {
                    connection.close();
                }
                catch(e) {
                    console.error("Error while closing MongoDB connection: ", e);
                }
                connection = null;

                shared.db.remove(name);
            }

            resolve();
        });
    };
};
TriplexMongoDB.prototype.ERROR_DB_ALREADY_EXISTS = "Database Already Exists";


/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = TriplexMongoDB;